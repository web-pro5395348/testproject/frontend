import type { BuyStock } from './BuyStock'

const defaultBSPaperItem = {
  id: -1,
  name: '',
  price: 0,
  qty: 0,
  unit: '',
  buystockId: -1,
  buystock: null
}
type BSPaperItem = {
  id: number
  name: string
  price: number
  qty: number
  unit: string
  buystockId: number
  buystock?: BuyStock
}

export { type BSPaperItem, defaultBSPaperItem }
