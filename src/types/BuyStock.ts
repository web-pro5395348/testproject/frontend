type BuyStock = {
  id?: number
  name: string
  unit: string
  price: number
}
export type { BuyStock }
