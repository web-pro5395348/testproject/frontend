import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { BuyStock } from '@/types/BuyStock'
import buyStockService from '@/services/buyStock'
import { useLoadingStore } from './loading'

export const useStockBuyStore = defineStore('stockbuy', () => {
  const loadingStore = useLoadingStore()

  const buyStocks = ref<BuyStock[]>([])

  async function getBuyStocks() {
    try {
      loadingStore.doLoad()
      const res = await buyStockService.getBuyStocks()
      buyStocks.value = res.data

      loadingStore.finish()
    } catch (e) {
      console.log('Error')
      loadingStore.finish()
    }
  }
  return { buyStocks, getBuyStocks }
})
