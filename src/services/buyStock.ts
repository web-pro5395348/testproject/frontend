import type { BuyStock } from '@/types/BuyStock'
import http from './http'

function addBuyStock(buyStock: BuyStock) {
  return http.post('/buyStocks', buyStock)
}

function updateBuyStock(buyStock: BuyStock) {
  return http.patch(`/buyStocks/${buyStock.id}`, buyStock)
}

function delBuyStock(buyStock: BuyStock) {
  return http.delete(`/buyStocks/${buyStock.id}`)
}

function getBuyStock(id: number) {
  return http.get(`/buyStocks/${id}`)
}

function getBuyStocks() {
  return http.get('/buyStocks')
}

export default { addBuyStock, updateBuyStock, delBuyStock, getBuyStock, getBuyStocks }
